# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Bookmarks::Application.config.secret_key_base = '378cf128b0f1db98f59e63463361360e943f61c1ff19e493b86ed4ec9d2b5b23271c1bf41c9a887bc9995314fa321262f5dc218bf745918a095bdc9a55534d2f'
